<?php

namespace App\Http\Controllers;

use App\Models\Pekerja;
use Illuminate\Http\Request;

class PekerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pekerja = new Pekerja();
        $pekerja->user_id = $request->user_id;
        $pekerja->nama = $request->nama;
        $pekerja->tanggal_lahir = $request->tanggal_lahir;

        $pekerja->save();
        return response()->json([
            'pesan' => 'berhasil menambahkan data',
            'data' => $pekerja
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function show(Pekerja $pekerja)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function edit(Pekerja $pekerja)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pekerja $pekerja)
    {
        // 
    }
    public function updated(Request $request, $user_id)
    {
        $pekerja = Pekerja::where('user_id', $user_id)->first();
        if (!$pekerja) {
            return response()->json([
                'pesan' => 'Pekerja Tidak ditemukan'
            ]);
        }

        $pekerja->nama = $request->nama;
        $pekerja->tanggal_lahir = $request->tanggal_lahir;
        $pekerja->save();

        return response()->json([
            'pesan' => 'berhasil mengubah data',
            'update' => $pekerja
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pekerja $pekerja)
    {
        //
    }
}

<?php

use App\Http\Controllers\PekerjaController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('user', UserController::class);
Route::patch('update/{id}', [UserController::class, 'update']);
Route::post('login', [UserController::class, 'login']);

Route::apiResource('pekerja', PekerjaController::class);
Route::post('updated/{user_id}', [PekerjaController::class, 'updated']);

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();   
// });

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tanggalLahir = Carbon::createFromFormat('Y-m-d', '1999-01-01')->addYears(rand(0, 30))->format('Y-m-d');
        DB::table('pekerjas')->insert([
            'user_id' => 4,
            'nama' => 'Adybudi',
            'tanggal_lahir' => $tanggalLahir
        ]);
    }
}

<?php

use Produk as GlobalProduk;

class Kategori
{
    public $namaKategori;

    function setKategori($kategori)
    {
        $this->namaKategori = $kategori;
    }
}

class Produk extends Kategori
{

    public $namaProduk;

    function setProduk($produk)
    {
        $this->namaProduk = $produk;
    }
}

$data = new Produk();

$data->setProduk("Toyota");
$data->setKategori("Mobil");

echo "Produk " . $data->namaProduk . " dengan kategori " . $data->namaKategori;

//contoh inheritance
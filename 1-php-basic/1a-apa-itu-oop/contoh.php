<?php

class Produk
{
    public $name;
    public $qty;

    function setName($name)
    {
        $this->name = $name;
    }
    function getName()
    {
        return $this->name;
    }
    function setQty($qty)
    {
        $this->qty = $qty;
    }
    function getQty()
    {
        return $this->qty;
    }
}

$data = new Produk();
$data->setName('Toyota');
$data->setQty(2);

echo "Nama Mobil: " . $data->getName();
echo "<br>";
echo "Jumlah: " . $data->getQty();
//contoh oop